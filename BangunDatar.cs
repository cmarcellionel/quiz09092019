using System;

namespace QUIZ09092019
{
    class BangunDatar
    {
        public void LuasPersegi()
        {
            int sisi, luas;
            Console.Write("Masukkan nilai sisi: ");
            sisi = Convert.ToInt32(Console.ReadLine());
            luas = sisi*sisi;
            Console.WriteLine("Luas persegi adalah "+luas);
        }

        public void LuasSegitiga()
        {
            int tinggi, alas, luas;
            Console.Write("Masukkan nilai alas: ");
            alas = Convert.ToInt32(Console.ReadLine());        
            Console.Write("Masukkan nilai tinggi: ");
            tinggi = Convert.ToInt32(Console.ReadLine());    
            luas = tinggi*alas/2;
            Console.WriteLine("Luas segitiga adalah "+luas);
        }

        public void LuasLingkaran()
        {
            int radius, pi=22/7, luas;
            Console.Write("Masukkan nilai radius: ");
            radius = Convert.ToInt32(Console.ReadLine());
            luas = radius*radius*pi;
            Console.WriteLine("Luas lingkaran adalah "+luas);       
        }
    }
}
