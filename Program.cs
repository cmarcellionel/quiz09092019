﻿using System;

namespace QUIZ09092019
{
    class Program
    {
        static void Main(string[] args)
        {
        int pilihan;

        BangunDatar luas = new BangunDatar();
        BangunRuang volume = new BangunRuang();


        Console.WriteLine("Selamat Datang!");
        Console.WriteLine("---------------");
        Console.WriteLine("Ketik 1 untuk menghitung luas persegi");
        Console.WriteLine("Ketik 2 untuk menghitung luas segitiga");
        Console.WriteLine("Ketik 3 untuk menghitung luas lingkaran");
        Console.WriteLine("Ketik 4 untuk menghitung volume balok");
        Console.WriteLine("Ketik 5 untuk menghitung volume kubus");
        Console.Write("Pilihan anda: ");

        pilihan = Convert.ToInt32(Console.ReadLine());

        if (pilihan == 1){
            luas.LuasPersegi();
        }
        else if (pilihan == 2){
            luas.LuasSegitiga();
        }
        else if (pilihan == 3){
            luas.LuasLingkaran();
        }
        else if (pilihan == 4){
            volume.VolumeBalok();
        }
        else if (pilihan == 5){
            volume.VolumeKubus();
        }
        else
        {
            Console.WriteLine("Error");
        }
        }
    }
}
