using System;

namespace QUIZ09092019
{
    class BangunRuang
    {
        public void VolumeBalok()
        {
            int panjang, lebar, tinggi, volume;
            Console.Write("Masukkan nilai panjang: ");
            panjang = Convert.ToInt32(Console.ReadLine());
            Console.Write("Masukkan nilai lebar: ");
            lebar = Convert.ToInt32(Console.ReadLine());
            Console.Write("Masukkan nilai tinggi: ");
            tinggi = Convert.ToInt32(Console.ReadLine());
            volume = panjang*lebar*tinggi;
            Console.WriteLine("Volume balok adalah "+volume);
        }

        public void VolumeKubus()
        {
            int sisi, volume;
            Console.Write("Masukkan nilai sisi: ");
            sisi = Convert.ToInt32(Console.ReadLine());
            volume = sisi*sisi*sisi;
            Console.WriteLine("Volume kubus adalah "+volume);            
        }

    }
}